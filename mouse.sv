`timescale 1 ns / 1 ns
module mouse(CLOCK_24, CLOCK_27, CLOCK_50, CLOCK_300, KEY, SW, HEX0, HEX1, HEX2, HEX3, LEDG, LEDR, PS2_DAT, PS2_CLK);
   input [1:0]  CLOCK_24;
   input [1:0]  CLOCK_27;
   input        CLOCK_50;
   output reg   CLOCK_300;
   
   input [3:0]  KEY;
   
   input [9:0]  SW;
   
   output [6:0] HEX0;
   output [6:0] HEX1;
   output [6:0] HEX2;
   output [6:0] HEX3;
   
   output [7:0] LEDG;
   output [9:0] LEDR;
   
   inout        PS2_DAT;
   inout        PS2_CLK;
   


   ps2_mouse_test mousetest(CLOCK_24, CLOCK_27, CLOCK_50, CLOCK_300, KEY, SW, HEX0, HEX1, HEX2, HEX3, LEDG, LEDR, PS2_DAT, PS2_CLK);
   
   
   
endmodule
